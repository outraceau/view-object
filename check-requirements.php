<?php
set_error_handler(function() {
    print(
        'Remember to install this library from the WordPress install root directory!' . PHP_EOL
        . 'Refer to decumentation for instructions on setting up this library with WordPress.' . PHP_EOL
        . 'https://bitbucket.org/outraceau/view-object/overview' . PHP_EOL
    );
});

/** Attempt to load the WordPress Environment and Template */
include_once( dirname( __FILE__ ) . '/wp-blog-header.php' );

restore_error_handler();
