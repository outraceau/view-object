<?php
namespace Outrace;

class View
{
    /**
     * Data associated with the view
     * 
     * @var array
     */
    private $data = [];

    /**
     * View template name, e.g. front-page.v.php
     *
     * @var string
     */
    private $viewTemplate;

    /**
     * Base path from which VIEWS_FOLDER_NAME can be accessed
     *
     * @var string
     */
    private $baseDir;

    const VIEWS_FOLDER_NAME = 'views';

    /**
     * Render the view object (template with data)
     *
     * @param string $viewTemplate
     * @param array $data
     * @param string $baseDir
     * @return void
     */
    public static function render($viewTemplate, $data = [], $baseDir = '.')
    {
        echo self::buildView($viewTemplate, $data, $baseDir);
    }

    /**
     * Build and return a view object
     *
     * @param string $viewTemplate
     * @param array $data
     * @param string $baseDir
     * @return $this
     */
    public static function buildView($viewTemplate, $data = [], $baseDir = '.')
    {
        return new static($viewTemplate, $data, $baseDir);
    }
    
    /**
     *
     * @param string $viewTemplate
     * @param array $data
     * @param string $baseDir
     */
    private function __construct($viewTemplate, $data = [], $baseDir = '.')
    {
        $this->viewTemplate = $viewTemplate;
        $this->data = $data;
        $this->baseDir = $baseDir;
    }
    
    /**
     * @param string $key
     * @return any
     */
    public function __get($key)
    {
        return $this->data[$key];
    }
    
    /**
     * @return string
     */
    public function __toString()
    {
        extract($this->data);
        chdir($this->baseDir . '/' . self::VIEWS_FOLDER_NAME);

        ob_start();
        include basename($this->viewTemplate);
        chdir(__DIR__); // Reset PHP's current directory to avoid side effects

        return ob_get_clean();
    }
}
