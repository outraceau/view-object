<?php
/**
 * Throw-away file, used only during local development of the library
 */

/**
 * Function mock
 * 
 * We're mocking WordPress environment
 */
function get_stylesheet_directory()
{
    return __DIR__;
}

function __autoload($classname) {
    $filename = substr($classname, strpos($classname, '\\') + 1) . '.php';

    require($filename);
}

require './functions.php';

renderView('view.v.php', ['var' => 'some_value']);
