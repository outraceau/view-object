<?php
/**
 * Wrapper function for WordPress to render the view object (template with data)
 *
 * @param string $view
 * @param array $data
 * @return void
 */
function renderView($view, $data = [], $baseDir = '.')
{
    if (!function_exists('get_stylesheet_directory')) {
        throw new Exception('This library can only be used with WordPress environment loaded.');
    }
    $baseDir = get_stylesheet_directory();
    Outrace\View::render($view, $data, $baseDir);
}
