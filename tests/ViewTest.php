<?php
use PHPUnit\Framework\TestCase;
use Outrace\View;

class ViewTest extends TestCase
{
    const TEST_VIEW_TEMPLATE_FILE = 'test-view.v.php';

    /**
     * System under test
     *
     * @var View
     */
    private $viewObject;

    public function setUp()
    {
       $this->viewObject = View::buildView(
           self::TEST_VIEW_TEMPLATE_FILE,
           [
               'var1' => 'value1',
               'var2' => 'value2',
           ]
        );
    }

    /**
     * @test
     *
     * @return void
     */
    public function dataPassedToTemplateIsAvailable()
    {
       $this->assertEquals($this->viewObject->var1, 'value1');
       $this->assertEquals($this->viewObject->var2, 'value2');
    }

    /**
     * @test
     *
     * @return void
     */
    public function templateContentIsRendered()
    {        
        View::render(self::TEST_VIEW_TEMPLATE_FILE, [], './tests');

        $this->expectOutputString('View template test');       
    }

    /**
     * @test
     *
     * @return void
     */
    public function renderedTemplateEqualsToViewObjectCastToString()    
    {
        ob_start();
        echo $this->viewObject;
        $viewObjectCastToString = ob_get_contents();
        ob_end_clean();

        ob_start();
        View::render(self::TEST_VIEW_TEMPLATE_FILE, [], './tests');
        $renderedTemplateString = ob_get_contents();
        ob_end_clean();
        
        $this->assertEquals($viewObjectCastToString, $renderedTemplateString);
    }
}
